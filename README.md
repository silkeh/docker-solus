# Solus Docker images

Unofficial builds of Docker images based on Solus.
All images are built in [GitLab CI] and pushed to [Docker Hub].

The Docker tags are as follows:

- [`base`]:     Solus image with the `system.base` component installed.
- [`slim`]:     `base` extended with essential tools.
- [`devel`]:    `slim` extended with the `system.devel` component.
- [`ypkg`]:     `devel` extended with `git` and `ccache`.
- [`iso`]:      `base` extended with tools for building ISOs.
- [`solbuild`]: `devel` extended with `solbuild`.
  Note: `solbuild` doesn't work in a non-privileged setting.

[GitLab CI]: https://gitlab.com/silkeh/docker-solus
[Docker Hub]: https://hub.docker.com/r/silkeh/solus/
[`base`]: https://gitlab.com/silkeh/docker-solus/-/blob/master/Dockerfile.base
[`devel`]: https://gitlab.com/silkeh/docker-solus/-/blob/master/Dockerfile.devel
[`ypkg`]: https://gitlab.com/silkeh/docker-solus/-/blob/master/Dockerfile.ypkg
[`iso`]: https://gitlab.com/silkeh/docker-solus/-/blob/master/Dockerfile.iso
[`solbuild`]: https://gitlab.com/silkeh/docker-solus/-/blob/master/Dockerfile.solbuild
